package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.HeroKuappObjetcs;

import net.thucydides.core.annotations.Step;

public class HeroKuappSteps 
{
	HeroKuappObjetcs heroKuappObjetcs;
	
	@Step
	public void IngresarPaginaWeb()
	{
		heroKuappObjetcs.open();
	}
	
	@Step
	public void SeleccionarLink()
	{
		heroKuappObjetcs.AbrirLink();
	}
	
	@Step
	public void Interactuarcontroles()
	{
		heroKuappObjetcs.AlertJS();
		heroKuappObjetcs.JSConfirm();
		heroKuappObjetcs.JSPrompt();
	}
	
	@Step
	public void VerificarResultado()
	{
		heroKuappObjetcs.VerificarResultado();
	}
	

}
