package com.choucair.formacion.pageobjects;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://the-internet.herokuapp.com/")

public class HeroKuappObjetcs extends PageObject
{

	@FindBy(xpath="//A[@href='/javascript_alerts'][text()='JavaScript Alerts']")
	private WebElementFacade JavaAlerts;
	
	@FindBy(xpath="//BUTTON[@onclick='jsAlert()'][text()='Click for JS Alert']")
	private WebElementFacade JSAlert;
	
	@FindBy(xpath="//BUTTON[@onclick='jsConfirm()'][text()='Click for JS Confirm']")
	private WebElementFacade JSConfirm;
	
	@FindBy(xpath="//BUTTON[@onclick='jsPrompt()'][text()='Click for JS Prompt']")
	private WebElementFacade JSPrompt;
	
	@FindBy(id="result")
	private WebElementFacade Resultado;
	
	
	public void AbrirLink()
	{
		JavaAlerts.click();
	}
	
	public void AlertJS()
	{
		JSAlert.click();
		getDriver().switchTo().alert().accept();
	}
	
	public void JSConfirm()
	{
		JSConfirm.click();
		getDriver().switchTo().alert().dismiss();
	}
	
	public void JSPrompt()
	{
		JSPrompt.click();	
		getDriver().switchTo().alert().sendKeys("Hola Mundo");
		getDriver().switchTo().alert().accept();
	}
	
	public void VerificarResultado()
	{
		assertThat(Resultado.getText(), containsString("You entered: Hola Mundo"));
	}
	

}
