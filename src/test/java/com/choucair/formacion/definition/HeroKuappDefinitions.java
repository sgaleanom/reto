package com.choucair.formacion.definition;

import com.choucair.formacion.steps.HeroKuappSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class HeroKuappDefinitions 
{
	@Steps
	HeroKuappSteps heroKuappSteps;
	
	@Given("^Abrir la pagina HeroKuapp$")
	public void Abrir_la_pagina_HeroKuapp()
	{
		heroKuappSteps.IngresarPaginaWeb();
	}
	
	@When("^Seleccionar el link javascript_alerts$")
	public void Seleccionar_el_link_javascript_alerts()
	{
		heroKuappSteps.SeleccionarLink();
	}
	
	@When("^Interactuar con los controles que hay en la pantalla$")
	public void Interactuar_con_los_controles_que_hay_en_la_pantalla()
	{
		heroKuappSteps.Interactuarcontroles();
	}
	
	@Then("^Aprende a manejar Alertas tipo JavaScript$")
	public void Aprende_a_manejar_Alertas_tipo_JavaScript()
	{
		heroKuappSteps.VerificarResultado();
	}
	

}
