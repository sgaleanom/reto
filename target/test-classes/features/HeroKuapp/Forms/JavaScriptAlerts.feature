#Author: sgaleanom@choucairtesting.com
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@Regresion
Feature: Uso de alertas JavaScript
  El usuario debe manejar los diferentes tipos de alertas JavaScript

  @CasoExitoso
  Scenario: Manejo de alertas JavaScript
    Given Abrir la pagina HeroKuapp
    When Seleccionar el link javascript_alerts
    And Interactuar con los controles que hay en la pantalla
		Then Aprende a manejar Alertas tipo JavaScript


